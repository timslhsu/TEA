﻿#property copyright "Copyright 2017, Tim Hsu"
#property link      ""
#property version   "1.2"
#property description "提姆茶７號"
#property description "海龜交易策略"
#property strict
#include <TEA.mqh>

//使用者輸入參數
input string CUSTOM_COMMENT       = "【提姆茶７號 v1.2】";     //畫面註解
input string BREAK_LINE_1         = "＝＝＝＝＝";              //＝ [ 進場控制 ] ＝＝＝＝＝＝
input string TRADE_DAYS           = "0123456";                 //操作日 (星期 0123456)
input int    TRADE_START_HOUR     = 0;                         //操作開始時間
input int    TRADE_END_HOUR       = 23;                        //操作結束時間
input string BREAK_LINE_3         = "＝＝＝＝＝";              //＝ [ 海龜參數 ] ＝＝＝＝＝＝
input int    ATR_RANGE            = 20;                        //ATR 區間
input int    S1_RANGE             = 20;                        //System 1 突破區間
input int    S1_TAKE_PROFIT_RANGE = 10;                        //System 1 停利區間
input int    S2_RANGE             = 55;                        //System 2 突破區間
input int    S2_TAKE_PROFIT_RANGE = 20;                        //System 2 停利區間
input double ADD_ON_ATR           = 0.5;                       //加碼距離 (幾個 ATR)
input double STOP_LOSS_ATR        = 2;                         //停損距離 (幾個 ATR)
input int    MAX_ORDERS           = 4;                         //最大單數
input double UNIT_RISK_PERCENTAGE = 1;                         //每單位價值(佔總資金比例)
input double MAX_LOTS             = 0;                         //每單最大手數 (0: 不限制)

//EA 相關
const int    MAGIC_NUMBER         = 930214;
const string ORDER_COMMENT_PREFIX = "TEA7_";    //交易單說明前置字串


//資訊顯示用的 Label 物件名稱
const string LBL_COMMENT                  = "lblComment";
const string LBL_TRADE_ENV                = "lblTradEvn";
const string LBL_PRICE                    = "lblPrice";
const string LBL_ATR                      = "lblAtr";
const string LBL_SERVER_TIME              = "lblServerTime";
const string LBL_LOCAL_TIME               = "lblLocalTime";
const string LBL_TRADE_TIME               = "lblTradeTime";
const string LN_S1_UPPER                  = "lnS1Upper";
const string LN_S1_LOWER                  = "lnS1Lower";
const string LN_S2_UPPER                  = "lnS2Upper";
const string LN_S2_LOWER                  = "lnS2Lower";
const string LN_TAKE_PROFIT               = "lnTakeProfit";
const string LN_STOP_LOSS                 = "lnStopLoss";
const string LN_ADD_ON                    = "lnAddOn";
const string TRADE_TIME_MSG               = "茶棧已打烊，明日請早！";
const string ENTRY_SYSTEM_NONE            = "NONE";
const string ENTRY_SYSTEM1                = "System1";
const string ENTRY_SYSTEM2                = "System2";
const int    BREAK_THROUGH_RESULT_WIN     = 1;
const int    BREAK_THROUGH_RESULT_LOSE    = -1;
const int    BREAK_THROUGH_RESULT_UNKNOWN = 0;

//全域變數
static bool        gs_isTradeTime;
static string      gs_symbol;
static long        gs_chartId;
static int         gs_atr;
static double      gs_s1Upper;
static double      gs_s1Lower;
static double      gs_s2Upper;
static double      gs_s2Lower;
static double      gs_addOnPrice;
static double      gs_takeProfitPrice;
static double      gs_stopLossPrice;
static OrderStruct gs_buyPosition[];
static OrderStruct gs_sellPosition[];
static string      gs_currentEntrySystem;
static int         gs_currentEntryAtr;


int OnInit() {
    _logger = CLog4mql::getInstance();

    if(AccountInfoString(ACCOUNT_CURRENCY) != "USD" || SymbolInfoString(Symbol(), SYMBOL_CURRENCY_PROFIT) != "USD") {
        MessageBox("必須使用美金計價之帳戶，\n並且交易貨幣必須為美金直接報價 (e.g. XXXUSD)!", "提示", MB_OK | MB_ICONERROR);
        return INIT_AGENT_NOT_SUITABLE;
    }
    
    if(Period() < PERIOD_D1) {
        MessageBox("預設參數僅適用於以 D1 時區執行海龜交易策略，\n若使用於其他時區，請自行對參數進行相應調整。", "提示", MB_OK | MB_ICONEXCLAMATION);
    }
    
    gs_symbol = Symbol();
    gs_chartId = ChartID();
    gs_isTradeTime = IsTradeTime(TRADE_DAYS, TRADE_START_HOUR, TRADE_END_HOUR);

    gs_atr = GetAtr(ATR_RANGE, 1);
    gs_s1Upper = GetDonchain(S1_RANGE, 1, MODE_UPPER);
    gs_s1Lower = GetDonchain(S1_RANGE, 1, MODE_LOWER);
    gs_s2Upper = GetDonchain(S2_RANGE, 1, MODE_UPPER);
    gs_s2Lower = GetDonchain(S2_RANGE, 1, MODE_LOWER);
    gs_currentEntrySystem = ENTRY_SYSTEM_NONE;
    gs_currentEntryAtr = 0;
    
    CalibrateCurrentPosition();
    _logger.debug(__FILE__, __LINE__, StringFormat("Current position: BUY = %d, SELL = %d", ArraySize(gs_buyPosition), ArraySize(gs_sellPosition)));

    PutInfoLables();
    UpdateInfoLabels();
    DrawTradeLines();
    SetTradeTimeLabel(gs_isTradeTime);

    return INIT_SUCCEEDED;
}


void OnDeinit(const int reason) {
    ObjectsDeleteAll(gs_chartId);
    _logger.release();
}


void OnTick() {
    if(HasNewBar()) {
        gs_atr = GetAtr(ATR_RANGE, 1);
        gs_s1Upper = GetDonchain(S1_RANGE, 1, MODE_UPPER);
        gs_s1Lower = GetDonchain(S1_RANGE, 1, MODE_LOWER);
        gs_s2Upper = GetDonchain(S2_RANGE, 1, MODE_UPPER);
        gs_s2Lower = GetDonchain(S2_RANGE, 1, MODE_LOWER);
        
        CalibrateCurrentPosition();
        DrawTradeLines();
    }

    gs_isTradeTime = IsTradeTime(TRADE_DAYS, TRADE_START_HOUR, TRADE_END_HOUR);
    UpdateInfoLabels();

    /*
    1. 檢查是否達到停損點
    2. 檢查是否達到停利點
    3. 檢查是否達到加碼點
    */
    //Buy 單管控
    if(ArraySize(gs_buyPosition) > 0) {
        //stop loss
        if(Bid <= gs_stopLossPrice) {
            _logger.debug(__FILE__, __LINE__, StringFormat("BUY STOP LOSS (Bid: %.5f, SL: %.5f), closing BUY position.", Bid, gs_stopLossPrice));
            CloseMarketOrders(gs_buyPosition);
            CalibrateCurrentPosition();
            DrawTradeLines();
        }    

        //take profit
        else if(Bid <= gs_takeProfitPrice) {
            _logger.debug(__FILE__, __LINE__, StringFormat("BUY TAKE PROFIT (Bid: %.5f, TP: %.5f), closing BUY position.", Bid, gs_takeProfitPrice));
            CloseMarketOrders(gs_buyPosition);
            CalibrateCurrentPosition();
            DrawTradeLines();
        }
        
        //add on when in trading time
        else if(gs_isTradeTime && Ask >= gs_addOnPrice && ArraySize(gs_buyPosition) < MAX_ORDERS) {
            _logger.debug(__FILE__, __LINE__, StringFormat("BUY ADD ON (Ask: %.5f, Addon: %.5f), sending BUY order...", Ask, gs_addOnPrice));
            int ticket = SendOrder(gs_symbol, OP_BUY, Ask, gs_buyPosition[0].lots, BuildOrderComment(OP_BUY, gs_currentEntrySystem, ArraySize(gs_buyPosition) + 1, gs_currentEntryAtr), MAGIC_NUMBER);
            if(ticket > 0) {
                CalibrateCurrentPosition();
                DrawTradeLines();
            }    
        }
    }

    //Sell 單管控
    else if(ArraySize(gs_sellPosition) > 0) {
        //stop loss
        if(Ask >= gs_stopLossPrice) {
            _logger.debug(__FILE__, __LINE__, StringFormat("SELL STOP LOSS (Ask: %.5f, SL: %.5f), closing SELL position.", Ask, gs_stopLossPrice));
            CloseMarketOrders(gs_sellPosition);
            CalibrateCurrentPosition();
            DrawTradeLines();
        }
        
        //take profit
        else if(Ask >= gs_takeProfitPrice) {
            _logger.debug(__FILE__, __LINE__, StringFormat("SELL TAKE PROFIT (Ask: %.5f, TP: %.5f), closing SELL position.", Ask, gs_takeProfitPrice));
            CloseMarketOrders(gs_sellPosition);
            CalibrateCurrentPosition();
            DrawTradeLines();
        }
        
        //add on when in trading time
        else if(gs_isTradeTime && Bid <= gs_addOnPrice && ArraySize(gs_sellPosition) < MAX_ORDERS) {
            _logger.debug(__FILE__, __LINE__, StringFormat("SELL ADD ON (Bid: %.5f, Addon: %.5f), sending SELL order...", Bid, gs_addOnPrice));
            int ticket = SendOrder(gs_symbol, OP_SELL, Bid, gs_sellPosition[0].lots, BuildOrderComment(OP_SELL, gs_currentEntrySystem, ArraySize(gs_sellPosition) + 1, gs_currentEntryAtr), MAGIC_NUMBER);
            if(ticket > 0) {
                CalibrateCurrentPosition();
                DrawTradeLines();
            }    
        }
    }
    
    //初始單
    else {
        /*
        1. 檢查是否在執行時間內
        2. 檢查是否發生突破
        */

        if(!gs_isTradeTime)  return;
        
        //ERURSD 每小點(pip)價值 USD$1, 所以可以直接用餘額除以 ATR 算出可交易點數
        //間接報價的(e.g. usdjpy) 要用 floor(accountBlance / atr*power(10, 5-digits)/close[0]) * 1%
        double unitLots = NormalizeDouble(MathFloor(AccountBalance() / gs_atr) * (UNIT_RISK_PERCENTAGE / 100), 2);
        if(unitLots < MarketInfo(gs_symbol, MODE_MINLOT)) {
            _logger.info(__FILE__, __LINE__, StringFormat("Unit lots %.2f (balance: %.2f, ATR: %d) less than minimum lots, REJECT to place any order.", unitLots, AccountBalance(), gs_atr));
            return;
        }
        if(MAX_LOTS > 0 && unitLots > MAX_LOTS) {
            unitLots = MAX_LOTS;
            _logger.debug(__FILE__, __LINE__, StringFormat("Unit lots %.2f (balance: %.2f, ATR: %d) exceed maximum lots %.2f, set to maximum lots.", unitLots, AccountBalance(), gs_atr, MAX_LOTS));
        }
        
        //Buy break through
        if(ArraySize(gs_buyPosition) == 0 && ((Ask > gs_s1Upper && LastS1BreakThroughResult(OP_BUY) != BREAK_THROUGH_RESULT_WIN) || Ask > gs_s2Upper)) {
            gs_currentEntryAtr = gs_atr;
            if(Ask > gs_s2Upper) {
                gs_currentEntrySystem = ENTRY_SYSTEM2;
            } else {
                gs_currentEntrySystem = ENTRY_SYSTEM1;
            }    
            
            _logger.debug(__FILE__, __LINE__, StringFormat("BUY %s break through found (Ask: %.5f, S1 upper: %.5f, S2 upper: %.5f), sending BUY order...", gs_currentEntrySystem, Ask, gs_s1Upper, gs_s2Upper));
            int ticket = SendOrder(gs_symbol, OP_BUY, Ask, unitLots, BuildOrderComment(OP_BUY, gs_currentEntrySystem, 1, gs_currentEntryAtr), MAGIC_NUMBER);
        
            if(ticket > 0) {
                CalibrateCurrentPosition();
                DrawTradeLines();
            }    
        }
        
        //Sell break through
        else if(ArraySize(gs_sellPosition) == 0 && ((Bid < gs_s1Lower && LastS1BreakThroughResult(OP_SELL) != BREAK_THROUGH_RESULT_WIN) || Bid < gs_s2Lower)) {
            gs_currentEntryAtr = gs_atr;
            if(Bid < gs_s2Lower) {
                gs_currentEntrySystem = ENTRY_SYSTEM2;
            } else {
                gs_currentEntrySystem = ENTRY_SYSTEM1;
            }    
            
            _logger.debug(__FILE__, __LINE__, StringFormat("SELL %s break through found (Bid: %.5f, S1 upper: %.5f, S2 upper: %.5f), sending SELL order...", gs_currentEntrySystem, Bid, gs_s1Lower, gs_s2Lower));
            int ticket = SendOrder(gs_symbol, OP_SELL, Bid, unitLots, BuildOrderComment(OP_SELL, gs_currentEntrySystem, 1, gs_currentEntryAtr), MAGIC_NUMBER);
        
            if(ticket > 0) {
                CalibrateCurrentPosition();
                DrawTradeLines();
            }    
        }
    }
}


//校準目前持倉狀況及各項監看價位
void CalibrateCurrentPosition() {
    CollectOrders(gs_symbol, OP_BUY, MAGIC_NUMBER, gs_buyPosition);
    CollectOrders(gs_symbol, OP_SELL, MAGIC_NUMBER, gs_sellPosition);

    if(ArraySize(gs_buyPosition) > 0) {
        gs_currentEntrySystem = ParseEntrySystem(gs_buyPosition[0].comment);
        CalcTradePrice(gs_buyPosition, OP_BUY);
    }
    
    else if(ArraySize(gs_sellPosition) > 0) {
        gs_currentEntrySystem = ParseEntrySystem(gs_sellPosition[0].comment);
        CalcTradePrice(gs_sellPosition, OP_SELL);
    }

    else
        gs_currentEntrySystem = ENTRY_SYSTEM_NONE;
}

//計算目前持倉的停損, 停利, 加碼點位
void CalcTradePrice(OrderStruct& orders[], int orderType) {
    int lastIdx = ArraySize(orders) - 1;
    
    if(lastIdx < 0) {
        _logger.error(__FILE__, __LINE__, "No open orders available.");
        return;
    }
    
    if(orderType == OP_BUY) {
        gs_currentEntryAtr = ParseEntryAtr(orders[lastIdx].comment);
        gs_addOnPrice = orders[lastIdx].openPrice + gs_currentEntryAtr * ADD_ON_ATR * Point;
        gs_stopLossPrice = orders[lastIdx].openPrice - gs_currentEntryAtr * STOP_LOSS_ATR * Point;
        
        if(gs_currentEntrySystem == ENTRY_SYSTEM1) {
            gs_takeProfitPrice = GetDonchain(S1_TAKE_PROFIT_RANGE, 1, MODE_LOWER);
        } else if(gs_currentEntrySystem == ENTRY_SYSTEM2) {
            gs_takeProfitPrice = GetDonchain(S2_TAKE_PROFIT_RANGE, 1, MODE_LOWER);
        }
        _logger.debug(__FILE__, __LINE__, StringFormat("BUY %s position: %d, ATR: %d, Addon: %.5f, SL: %.5f, TP: %.5f", gs_currentEntrySystem, ArraySize(orders), gs_currentEntryAtr, gs_addOnPrice, gs_stopLossPrice, gs_takeProfitPrice));
    }

    else {
        gs_currentEntryAtr = ParseEntryAtr(orders[lastIdx].comment);
        gs_addOnPrice = orders[lastIdx].openPrice - gs_currentEntryAtr * ADD_ON_ATR * Point;
        gs_stopLossPrice = orders[lastIdx].openPrice + gs_currentEntryAtr * STOP_LOSS_ATR * Point;
        
        if(gs_currentEntrySystem == ENTRY_SYSTEM1) {
            gs_takeProfitPrice = GetDonchain(S1_TAKE_PROFIT_RANGE, 1, MODE_UPPER);
        } else if(gs_currentEntrySystem == ENTRY_SYSTEM2) {
            gs_takeProfitPrice = GetDonchain(S2_TAKE_PROFIT_RANGE, 1, MODE_UPPER);
        }
        _logger.debug(__FILE__, __LINE__, StringFormat("SELL %s position: %d, ATR: %d, Addon: %.5f, SL: %.5f, TP: %.5f", gs_currentEntrySystem, ArraySize(orders), gs_currentEntryAtr, gs_addOnPrice, gs_stopLossPrice, gs_takeProfitPrice));
    }
}


//取得 ATR 值
int GetAtr(int range, int shift) {
    return PriceToInteger(iATR(gs_symbol, Period(), range, shift));
}


//取得 Donchain 值
double GetDonchain(int range, int shift, int mode) {
    return iDonchain(gs_symbol, Period(), range, 0, mode, shift);
}


//交易單註解
string BuildOrderComment(int orderType, string entrySystem, int orderSeq, int atr) {
    string cmmt = ORDER_COMMENT_PREFIX;
    
    if(orderType == OP_BUY) {
        if(entrySystem == ENTRY_SYSTEM1)  cmmt += "_S1";
        if(entrySystem == ENTRY_SYSTEM2)  cmmt += "_S2";
        return cmmt + "_B-" + (string)orderSeq + "_" + (string)atr;
    }

    if(orderType == OP_SELL) {
        if(entrySystem == ENTRY_SYSTEM1)  cmmt += "_S1";
        if(entrySystem == ENTRY_SYSTEM2)  cmmt += "_S2";
        return cmmt + "_S-" + (string)orderSeq + "_" + (string)atr;
    }

    return NULL;
}


//解析持倉單的入場條件
string ParseEntrySystem(string orderComment) {
    string cmmt = ORDER_COMMENT_PREFIX + "_S";
    StringReplace(orderComment, cmmt, "");
    int t = (int)StringToInteger(StringSubstr(orderComment, 0, 1));
    
    switch(t) {
        case 1:
            return ENTRY_SYSTEM1;
        case 2:
            return ENTRY_SYSTEM2;
        default:
            return ENTRY_SYSTEM_NONE;    
    }
}


//解析持倉單的入場條件
int ParseEntryAtr(string orderComment) {
    string tmp[];
    StringSplit(orderComment, '_', tmp);
    return (int)StringToInteger(tmp[ArraySize(tmp) - 1]);
}


//在圖表上安置各項資訊標籤物件
void PutInfoLables() {
    ObjectsDeleteAll(gs_chartId);

    //comment label
    ObjectCreate(gs_chartId, LBL_COMMENT, OBJ_LABEL, 0, 0, 0);
    ObjectSetInteger(gs_chartId, LBL_COMMENT, OBJPROP_CORNER, CORNER_RIGHT_UPPER);
    ObjectSetInteger(gs_chartId, LBL_COMMENT, OBJPROP_ANCHOR, ANCHOR_RIGHT);
    ObjectSetInteger(gs_chartId, LBL_COMMENT, OBJPROP_XDISTANCE, 5);
    ObjectSetInteger(gs_chartId, LBL_COMMENT, OBJPROP_YDISTANCE, 24);
    ObjectSetInteger(gs_chartId, LBL_COMMENT, OBJPROP_COLOR, clrYellow);
    ObjectSetInteger(gs_chartId, LBL_COMMENT, OBJPROP_FONTSIZE, 12);
    ObjectSetString(gs_chartId, LBL_COMMENT, OBJPROP_FONT, "微軟正黑體");
    string custComment = CUSTOM_COMMENT;
    ENUM_ACCOUNT_TRADE_MODE accountType = (ENUM_ACCOUNT_TRADE_MODE)AccountInfoInteger(ACCOUNT_TRADE_MODE);
    switch(accountType) {
        case ACCOUNT_TRADE_MODE_DEMO:
            custComment += "模擬倉";
            break;
        case ACCOUNT_TRADE_MODE_REAL:
            custComment += "真倉";
            break;
        default:
            break;
    }
    SetLabelText(gs_chartId, LBL_COMMENT, custComment);

    //交易品種及時區
    ObjectCreate(gs_chartId, LBL_TRADE_ENV, OBJ_LABEL, 0, 0, 0);
    ObjectSetInteger(gs_chartId, LBL_TRADE_ENV, OBJPROP_CORNER, CORNER_RIGHT_UPPER);
    ObjectSetInteger(gs_chartId, LBL_TRADE_ENV, OBJPROP_ANCHOR, ANCHOR_RIGHT);
    ObjectSetInteger(gs_chartId, LBL_TRADE_ENV, OBJPROP_XDISTANCE, 5);
    ObjectSetInteger(gs_chartId, LBL_TRADE_ENV, OBJPROP_YDISTANCE, 45);
    ObjectSetInteger(gs_chartId, LBL_TRADE_ENV, OBJPROP_COLOR, clrOrange);
    ObjectSetInteger(gs_chartId, LBL_TRADE_ENV, OBJPROP_FONTSIZE, 18);
    ObjectSetString(gs_chartId, LBL_TRADE_ENV, OBJPROP_FONT, "Verdana");
    SetLabelText(gs_chartId, LBL_TRADE_ENV, Symbol() + "-" + GetTimeFrameString(Period()));

    //價格
    ObjectCreate(gs_chartId, LBL_PRICE, OBJ_LABEL, 0, 0, 0);
    ObjectSetInteger(gs_chartId, LBL_PRICE, OBJPROP_CORNER, CORNER_RIGHT_UPPER);
    ObjectSetInteger(gs_chartId, LBL_PRICE, OBJPROP_ANCHOR, ANCHOR_RIGHT);
    ObjectSetInteger(gs_chartId, LBL_PRICE, OBJPROP_XDISTANCE, 5);
    ObjectSetInteger(gs_chartId, LBL_PRICE, OBJPROP_YDISTANCE, 72);
    ObjectSetInteger(gs_chartId, LBL_PRICE, OBJPROP_COLOR, clrDeepSkyBlue);
    ObjectSetInteger(gs_chartId, LBL_PRICE, OBJPROP_FONTSIZE, 24);
    ObjectSetString(gs_chartId, LBL_PRICE, OBJPROP_FONT, "Verdana");
    SetLabelText(gs_chartId, LBL_PRICE, StringFormat("%." + (string)Digits + "f", Close[0]));

    //ATR
    ObjectCreate(gs_chartId, LBL_ATR, OBJ_LABEL, 0, 0, 0);
    ObjectSetInteger(gs_chartId, LBL_ATR, OBJPROP_CORNER, CORNER_RIGHT_UPPER);
    ObjectSetInteger(gs_chartId, LBL_ATR, OBJPROP_ANCHOR, ANCHOR_RIGHT);
    ObjectSetInteger(gs_chartId, LBL_ATR, OBJPROP_XDISTANCE, 5);
    ObjectSetInteger(gs_chartId, LBL_ATR, OBJPROP_YDISTANCE, 98);
    ObjectSetInteger(gs_chartId, LBL_ATR, OBJPROP_COLOR, clrNavajoWhite);
    ObjectSetInteger(gs_chartId, LBL_ATR, OBJPROP_FONTSIZE, 12);
    ObjectSetString(gs_chartId, LBL_ATR, OBJPROP_FONT, "Verdana");
    SetLabelText(gs_chartId, LBL_ATR, StringFormat("ATR：%d", gs_atr));

    //主機時間
    ObjectCreate(gs_chartId, LBL_SERVER_TIME, OBJ_LABEL, 0, 0, 0);
    ObjectSetInteger(gs_chartId, LBL_SERVER_TIME, OBJPROP_CORNER, CORNER_RIGHT_LOWER);
    ObjectSetInteger(gs_chartId, LBL_SERVER_TIME, OBJPROP_ANCHOR, ANCHOR_RIGHT);
    ObjectSetInteger(gs_chartId, LBL_SERVER_TIME, OBJPROP_XDISTANCE, 5);
    ObjectSetInteger(gs_chartId, LBL_SERVER_TIME, OBJPROP_YDISTANCE, 30);
    ObjectSetInteger(gs_chartId, LBL_SERVER_TIME, OBJPROP_COLOR, clrLimeGreen);
    ObjectSetInteger(gs_chartId, LBL_SERVER_TIME, OBJPROP_FONTSIZE, 10);
    ObjectSetString(gs_chartId, LBL_SERVER_TIME, OBJPROP_FONT, "Verdana");
    SetLabelText(gs_chartId, LBL_SERVER_TIME, "主機：" + TimeToString(TimeCurrent(), TIME_DATE | TIME_MINUTES | TIME_SECONDS));

    //本機時間
    ObjectCreate(gs_chartId, LBL_LOCAL_TIME, OBJ_LABEL, 0, 0, 0);
    ObjectSetInteger(gs_chartId, LBL_LOCAL_TIME, OBJPROP_CORNER, CORNER_RIGHT_LOWER);
    ObjectSetInteger(gs_chartId, LBL_LOCAL_TIME, OBJPROP_ANCHOR, ANCHOR_RIGHT);
    ObjectSetInteger(gs_chartId, LBL_LOCAL_TIME, OBJPROP_XDISTANCE, 5);
    ObjectSetInteger(gs_chartId, LBL_LOCAL_TIME, OBJPROP_YDISTANCE, 15);
    ObjectSetInteger(gs_chartId, LBL_LOCAL_TIME, OBJPROP_COLOR, clrLimeGreen);
    ObjectSetInteger(gs_chartId, LBL_LOCAL_TIME, OBJPROP_FONTSIZE, 10);
    ObjectSetString(gs_chartId, LBL_LOCAL_TIME, OBJPROP_FONT, "Verdana");
    SetLabelText(gs_chartId, LBL_LOCAL_TIME, "本地：" + TimeToString(TimeLocal(), TIME_DATE | TIME_MINUTES | TIME_SECONDS));
}


//設定標籤文字內容
void SetLabelText(long chartId, string labelName, string labelText) {
    ObjectSetString(chartId, labelName, OBJPROP_TEXT, labelText);
}


//更新資訊標籤內容
void UpdateInfoLabels() {
    SetLabelText(gs_chartId, LBL_PRICE, StringFormat("%." + (string)Digits + "f", Close[0]));
    if(Close[0] > Open[0])
        ObjectSetInteger(gs_chartId, LBL_PRICE, OBJPROP_COLOR, clrDeepSkyBlue);
    else if(Close[0] < Open[0])
        ObjectSetInteger(gs_chartId, LBL_PRICE, OBJPROP_COLOR, clrDeepPink);
    else
        ObjectSetInteger(gs_chartId, LBL_PRICE, OBJPROP_COLOR, clrDarkGray);

    SetLabelText(gs_chartId, LBL_ATR, StringFormat("ATR：%d", gs_atr));
    
    SetLabelText(gs_chartId, LBL_SERVER_TIME, "主機：" + TimeToString(TimeCurrent(), TIME_DATE | TIME_MINUTES | TIME_SECONDS));
    SetLabelText(gs_chartId, LBL_LOCAL_TIME, "本地：" + TimeToString(TimeLocal(), TIME_DATE | TIME_MINUTES | TIME_SECONDS));

    SetTradeTimeLabel(gs_isTradeTime);
}


//控制顯示超過進場時間訊息
void SetTradeTimeLabel(bool isTradeTime) {
    if(isTradeTime) {
        ObjectDelete(gs_chartId, LBL_TRADE_TIME);

    } else {
        ObjectCreate(gs_chartId, LBL_TRADE_TIME, OBJ_LABEL, 0, 0, 0);
        ObjectSetInteger(gs_chartId, LBL_TRADE_TIME, OBJPROP_CORNER, CORNER_LEFT_UPPER);
        ObjectSetInteger(gs_chartId, LBL_TRADE_TIME, OBJPROP_ANCHOR, ANCHOR_LEFT);
        ObjectSetInteger(gs_chartId, LBL_TRADE_TIME, OBJPROP_XDISTANCE, 350);
        ObjectSetInteger(gs_chartId, LBL_TRADE_TIME, OBJPROP_YDISTANCE, 60);
        ObjectSetInteger(gs_chartId, LBL_TRADE_TIME, OBJPROP_COLOR, clrRed);
        ObjectSetInteger(gs_chartId, LBL_TRADE_TIME, OBJPROP_FONTSIZE, 24);
        ObjectSetString(gs_chartId, LBL_TRADE_TIME, OBJPROP_FONT, "微軟正黑體");
        SetLabelText(gs_chartId, LBL_TRADE_TIME, TRADE_TIME_MSG);
    }
}


//繪製各項突破線
void DrawTradeLines() {
    if(ArraySize(gs_buyPosition) == 0 && ArraySize(gs_sellPosition) == 0) {
        DrawLine(LN_S1_UPPER, gs_s1Upper, clrDodgerBlue);
        DrawLine(LN_S1_LOWER, gs_s1Lower, clrDarkOrange);
        DrawLine(LN_S2_UPPER, gs_s2Upper, clrDodgerBlue);
        DrawLine(LN_S2_LOWER, gs_s2Lower, clrDarkOrange);
        ObjectDelete(gs_chartId, LN_ADD_ON);
        ObjectDelete(gs_chartId, LN_TAKE_PROFIT);
        ObjectDelete(gs_chartId, LN_STOP_LOSS);
        
    } else {
        if(ArraySize(gs_buyPosition) < MAX_ORDERS && ArraySize(gs_sellPosition) < MAX_ORDERS)
            DrawLine(LN_ADD_ON, gs_addOnPrice, clrGold, STYLE_DASHDOT);
        else
            ObjectDelete(gs_chartId, LN_ADD_ON);
    
        DrawLine(LN_TAKE_PROFIT, gs_takeProfitPrice, clrRed, STYLE_DOT);
        DrawLine(LN_STOP_LOSS, gs_stopLossPrice, clrMediumSeaGreen, STYLE_DOT);
        ObjectDelete(gs_chartId, LN_S1_UPPER);
        ObjectDelete(gs_chartId, LN_S1_LOWER);
        ObjectDelete(gs_chartId, LN_S2_UPPER);
        ObjectDelete(gs_chartId, LN_S2_LOWER);
    }
}


//繪製線條
void DrawLine(string lineName, double linePrice, long lineColor, int lineStyle = STYLE_SOLID, int lineWidth = 1) {
    ObjectDelete(gs_chartId, lineName);
    ObjectCreate(gs_chartId, lineName, OBJ_HLINE, 0, TimeCurrent(), linePrice);
    ObjectSetInteger(gs_chartId, lineName, OBJPROP_COLOR, lineColor);
    ObjectSetInteger(gs_chartId, lineName, OBJPROP_STYLE, lineStyle);
    ObjectSetInteger(gs_chartId, lineName, OBJPROP_WIDTH, lineWidth);
}


int LastS1BreakThroughResult(int orderType) {
    int atr;
    double openPrice;
    double stopLoss;
    double takeProfit;
    for(int i = 12; i <= 1000; i++) {
        //buy breakthrough
        if(iHigh(Symbol(), Period(), i) > GetDonchain(S1_RANGE, i + 1, MODE_UPPER)) {
            _logger.debug(__FILE__, __LINE__, StringFormat("Found S1 BUY breakthrough on %s", TimeToString(iTime(Symbol(), Period(), i), TIME_DATE)));
            if(orderType != OP_BUY)  return BREAK_THROUGH_RESULT_UNKNOWN;
            
            atr = GetAtr(S1_RANGE, i + 1);
            openPrice = GetDonchain(S1_RANGE, i + 1, MODE_UPPER);
            stopLoss = openPrice - IntegerToPrice((int)(atr * STOP_LOSS_ATR));
            _logger.debug(__FILE__, __LINE__, StringFormat("ATR: %d, open: %.5f, SL: %.5f", atr, openPrice, stopLoss));
            for(int j = i; j >= 2; j--) {
                takeProfit = GetDonchain(S1_TAKE_PROFIT_RANGE, j + 1, MODE_LOWER);
                _logger.debug(__FILE__, __LINE__, StringFormat("TP: %.5f", takeProfit));
                if(stopLoss > takeProfit && iLow(Symbol(), Period(), j) < stopLoss) {  //停利比止損低, 先到止損
                    _logger.debug(__FILE__, __LINE__, StringFormat("Stop loss on %s", TimeToString(iTime(Symbol(), Period(), j), TIME_DATE)));
                    return BREAK_THROUGH_RESULT_LOSE;
                } else if(takeProfit > stopLoss && iLow(Symbol(), Period(), j) < takeProfit) {  //停利比止損高, 先到停利
                    if(takeProfit > openPrice) {
                        _logger.debug(__FILE__, __LINE__, StringFormat("Favoriable BUY TP on %s", TimeToString(iTime(Symbol(), Period(), j), TIME_DATE)));
                        return BREAK_THROUGH_RESULT_WIN;
                    } else {
                        _logger.debug(__FILE__, __LINE__, StringFormat("Unfavoriable BUY TP on %s", TimeToString(iTime(Symbol(), Period(), j), TIME_DATE)));
                        return BREAK_THROUGH_RESULT_LOSE;
                    }    
                }
            }

        //sell breakthrough
        } else if(iLow(Symbol(), Period(), i) < GetDonchain(S1_RANGE, i + 1, MODE_LOWER)) {
            _logger.debug(__FILE__, __LINE__, StringFormat("Found S1 SELL breakthrough on %s", TimeToString(iTime(Symbol(), Period(), i), TIME_DATE)));
            if(orderType != OP_SELL)  return BREAK_THROUGH_RESULT_UNKNOWN;
            
            atr = GetAtr(S1_RANGE, i + 1);
            openPrice = GetDonchain(S1_RANGE, i + 1, MODE_LOWER);
            stopLoss = openPrice + IntegerToPrice((int)(atr * STOP_LOSS_ATR));
            _logger.debug(__FILE__, __LINE__, StringFormat("ATR: %d, open: %.5f, SL: %.5f", atr, openPrice, stopLoss));
            for(int j = i; j >= 2; j--) {
                takeProfit = GetDonchain(S1_TAKE_PROFIT_RANGE, j + 1, MODE_UPPER);
                _logger.debug(__FILE__, __LINE__, StringFormat("TP: %.5f", takeProfit));
                if(stopLoss < takeProfit && iHigh(Symbol(), Period(), j) > stopLoss) {  //停利比止損高, 先到止損
                    _logger.debug(__FILE__, __LINE__, StringFormat("Stop loss on %s", TimeToString(iTime(Symbol(), Period(), j), TIME_DATE)));
                    return BREAK_THROUGH_RESULT_LOSE;
                } else if(takeProfit < stopLoss && iHigh(Symbol(), Period(), j) > takeProfit) {  //停利比止損低, 先到停利
                    if(takeProfit < openPrice) {
                        _logger.debug(__FILE__, __LINE__, StringFormat("Favoriable SELL TP on %s", TimeToString(iTime(Symbol(), Period(), j), TIME_DATE)));
                        return BREAK_THROUGH_RESULT_WIN;
                    } else {
                        _logger.debug(__FILE__, __LINE__, StringFormat("Unfavoriable SELL TP on %s", TimeToString(iTime(Symbol(), Period(), j), TIME_DATE)));
                        return BREAK_THROUGH_RESULT_LOSE;
                    }    
                }
            }

        } else {
            return BREAK_THROUGH_RESULT_UNKNOWN;
        }
    }
    
    return BREAK_THROUGH_RESULT_UNKNOWN;
}