#property copyright "Copyright 2016-2017, Tim Hsu"
#property link      ""
#property version   "1.1"
#property description "繪製歷史交易軌跡"
#property strict
#property script_show_inputs
#include <TEA.mqh>

enum enumDataSource {
    ACCOUNT_HISTORY,    //帳戶歷史
    EXTERNAL_FILE       //指定檔案
};

input enumDataSource TRADE_DATA_SOURCE  = ACCOUNT_HISTORY;       //交易資料來源
input string         TRADE_RECORDS_FILE = "TradeRecords.csv";    //交易資料檔名
input bool           DRAW_BUY_ORDERS    = true;                  //繪製 BUY 軌跡
input bool           DRAW_SELL_ORDERS   = true;                  //繪製 SELL 軌跡
input int            MAGIC_NUMBER       = 0;                     //繪製指定 Magic Number 的交易 (0: 全部交易)

static long gs_chartId = ChartID();

void OnStart() {
    _logger = CLog4mql::getInstance();
    
    ObjectsDeleteAll(gs_chartId);

    if(TRADE_DATA_SOURCE == EXTERNAL_FILE && !FileIsExist(TRADE_RECORDS_FILE, FILE_COMMON)) {
        Print("交易記錄檔路徑：" + TerminalInfoString(TERMINAL_COMMONDATA_PATH) + "\\Files");
        MessageBox("交易記錄檔必需置於 " + TerminalInfoString(TERMINAL_COMMONDATA_PATH) + "\\Files", "提示", MB_OK | MB_ICONEXCLAMATION);
    }
    
    if(DRAW_BUY_ORDERS) {
        OrderStruct buyOrders[];
        if(TRADE_DATA_SOURCE == ACCOUNT_HISTORY) 
            CollectHistoryOrders(Symbol(), OP_BUY, MAGIC_NUMBER, buyOrders);
        else
            CollectHistoryOrders(TRADE_RECORDS_FILE, Symbol(), OP_BUY, MAGIC_NUMBER, buyOrders);

        DrawTrades(buyOrders, OP_BUY);
    }

    if(DRAW_SELL_ORDERS) {
        OrderStruct sellOrders[];
        if(TRADE_DATA_SOURCE == ACCOUNT_HISTORY) 
            CollectHistoryOrders(Symbol(), OP_SELL, MAGIC_NUMBER, sellOrders);
        else
            CollectHistoryOrders(TRADE_RECORDS_FILE, Symbol(), OP_SELL, MAGIC_NUMBER, sellOrders);

        DrawTrades(sellOrders, OP_SELL);
    }
    
    _logger.release();
}


//繪製交易軌跡
void DrawTrades(OrderStruct& orders[], int orderType) {
    string desc;
    double profit;
    for(int i = 0; i < ArraySize(orders); i++) {
        desc = StringFormat("Lots %.2f  %s", orders[i].lots, orders[i].comment);
        DrawOpenArrow("O-" + (string)orders[i].ticket, orders[i].openTime, orders[i].openPrice, orders[i].orderType, desc);

        profit = NormalizeDouble(orders[i].profit + orders[i].swap, 2);
        desc = "";
        DrawLine("L-" + (string)orders[i].ticket, orders[i].openTime, orders[i].openPrice, orders[i].closeTime, orders[i].closePrice, orders[i].orderType, desc, (profit >= 0));
        
        desc = StringFormat("Profit $%.2f", profit);
        DrawCloseArrow("C-" + (string)orders[i].ticket, orders[i].closeTime, orders[i].closePrice, orders[i].orderType, desc);
    }
}


//畫進場點
void DrawOpenArrow(string arrowName, datetime arrowTime, double arrowPrice, int orderType, string description = "") {
    ENUM_OBJECT objType = (orderType == OP_BUY)? OBJ_ARROW_BUY : OBJ_ARROW_SELL;
    color orderColor = (orderType == OP_BUY)? clrBlue : clrRed;
    
    ObjectCreate(gs_chartId, arrowName, objType, 0, arrowTime, arrowPrice);
    ObjectSetInteger(gs_chartId, arrowName, OBJPROP_COLOR, orderColor);
    ObjectSetString(gs_chartId, arrowName, OBJPROP_TEXT, description);
}

//畫出場點
void DrawCloseArrow(string arrowName, datetime arrowTime, double arrowPrice, int orderType, string description = "") {
    ENUM_OBJECT objType = OBJ_ARROW_STOP;
    color orderColor = (orderType == OP_BUY)? clrBlue : clrRed;
    
    ObjectCreate(gs_chartId, arrowName, objType, 0, arrowTime, arrowPrice);
    ObjectSetInteger(gs_chartId, arrowName, OBJPROP_COLOR, orderColor);
    ObjectSetString(gs_chartId, arrowName, OBJPROP_TEXT, description);
}


//畫出進場點至出場點之間的連線
void DrawLine(string lineName, datetime lineOpenTime, double lineOpenPrice, datetime lineCloseTime, double lineClosePrice, int orderType, string description = "", bool profitable = true) {
    color orderColor = (orderType == OP_BUY)? clrDodgerBlue : clrOrangeRed;
    
    ObjectCreate(gs_chartId, lineName, OBJ_TREND, 0, lineOpenTime, lineOpenPrice, lineCloseTime, lineClosePrice);
    ObjectSetInteger(gs_chartId, lineName, OBJPROP_COLOR, orderColor);
    if(profitable) {
        ObjectSetInteger(gs_chartId, lineName, OBJPROP_STYLE, STYLE_SOLID);
        ObjectSetInteger(gs_chartId, lineName, OBJPROP_WIDTH, 2);
    } else {
        ObjectSetInteger(gs_chartId, lineName, OBJPROP_STYLE, STYLE_DASH);
        //ObjectSetInteger(gs_chartId, lineName, OBJPROP_WIDTH, 1);
    }
    ObjectSetString(gs_chartId, lineName, OBJPROP_TEXT, description);
    ObjectSet(lineName, OBJPROP_RAY, 0);
    ObjectSetString(gs_chartId, lineName, OBJPROP_TEXT, description);
}


//#Account,Ticket,Symbol,OrderType,OpenTime,OpenPrice,CloseTime,ClosePrice,Lots,Profit,TakeProfit,StopLoss,Swap,Commission,Comment,MagicNumber
//0        1      2      3         4        5         6         7          8    9      10         11       12   13         14      15
//從檔案中讀取交易記錄
bool CollectHistoryOrders(string fileName, string symbol, int orderType, int magicNumber, OrderStruct& orders[]) {
    if(!FileIsExist(fileName, FILE_COMMON)) {
        _logger.error(__FILE__, __LINE__, StringFormat("%s\\Files\\%s does not exist.", TerminalInfoString(TERMINAL_COMMONDATA_PATH), fileName));
        return false;
    }
    
    int file = FileOpen(fileName, FILE_COMMON | FILE_SHARE_READ | FILE_TXT);
    if(file == INVALID_HANDLE) {
        _logger.error(__FILE__, __LINE__, "Invalid file handel, unable to open file.");
        return false;
    }
    
    ArrayFree(orders);
    string line;
    string tmp[];
    int idx;
    while(!FileIsEnding(file)) {
        line = StringTrimLeft(StringTrimRight(FileReadString(file)));
        StringSplit(line, ',', tmp);
        if(tmp[2] == symbol && orderType == StringToInteger(tmp[3])) {
            if(magicNumber > 0 && tmp[15] != (string)magicNumber)  continue;
            
            ArrayResize(orders, ArraySize(orders) + 1, 25);
            idx = ArraySize(orders) - 1;
            orders[idx].ticket = StringToInteger(tmp[1]);
            orders[idx].symbol = tmp[2];
            orders[idx].orderType = StringToInteger(tmp[3]);
            orders[idx].openTime = StringToTime(tmp[4]);
            orders[idx].openPrice = StringToDouble(tmp[5]);
            orders[idx].closeTime = StringToTime(tmp[6]);
            orders[idx].closePrice = StringToDouble(tmp[7]);
            orders[idx].lots = StringToDouble(tmp[8]);
            orders[idx].profit = StringToDouble(tmp[9]);
            orders[idx].takeProfit = StringToDouble(tmp[10]);
            orders[idx].stopLoss = StringToDouble(tmp[11]);
            orders[idx].swap = StringToDouble(tmp[12]);
            orders[idx].commission = StringToDouble(tmp[13]);
            orders[idx].comment = tmp[14];
            orders[idx].magicNumber = StringToInteger(tmp[15]);
        }
    }
    
    FileClose(file);
    
    return true;
}