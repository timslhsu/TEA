﻿#property copyright "Copyright 2017, MetaQuotes Software Corp."
#property link      ""
#property version   "1.00"
#property description "由芝加哥期貨交易所 (CME) 下載 Open Interest 及期貨合約報價資料"
#property strict
#property script_show_inputs
#include <TEA.mqh>
#include <json.mqh>
#include <sqlite.mqh>

input int    DOWNLOAD_DAY_RANGE = 3;     //下載資料回溯天數

                                  //http://www.cmegroup.com/CmeWS/mvc/Volume/Details/F/58/20170228/F?tradeDate=20170228
const string CME_OI_END_POINT    = "http://www.cmegroup.com/CmeWS/mvc/Volume/Details/F/%d/%s/F?tradeDate=%s";
                                  //http://www.cmegroup.com/CmeWS/mvc/Settlements/Futures/Settlements/58/FUT?strategy=DEFAULT&tradeDate=02/28/2017
const string CME_PRICE_END_POINT = "http://www.cmegroup.com/CmeWS/mvc/Settlements/Futures/Settlements/%d/FUT?strategy=DEFAULT&tradeDate=%s";
const string CME_DB_FILE = "cme_oi.db";
const string CME_TABLE_NAME = "CmeOi";

struct OiDataStruct {
    string currency;
    string tradeDate;
    int monthId;
    string month;
    int openInterest;
    int oiChange;
    double open;
    double close;
    double high;
    double low;
};

void OnStart() {

    for(int i = 0; i < DOWNLOAD_DAY_RANGE; i++){
        OiDataStruct allOiData[];
        string dtOi = Replace(TimeToString(TimeCurrent() - 86400 * i, TIME_DATE), ".", "");
        string dtPrice = StringSubstr(dtOi, 4, 2) + "/" + StringSubstr(dtOi, 6, 2) + "/" + StringSubstr(dtOi, 0, 4);
        
        string url, oiResp, priceResp;
        //OI
        url = StringFormat(CME_OI_END_POINT, GetCurrencyCode(Symbol()), dtOi, dtOi);
        Print("CME OI URL: ", url);
        oiResp = CrawlOiData(url);
        //Price
        url = StringFormat(CME_PRICE_END_POINT, GetCurrencyCode(Symbol()), dtPrice);
        Print("CME Price URL: ", url);
        priceResp = CrawlOiData(url);
        
        ParseOiData(Symbol(), oiResp, priceResp, allOiData);
        WriteOiToDB(CME_DB_FILE, allOiData);
    }
}

int GetCurrencyCode(string symbol) {
    if(symbol == "EURUSD")  return 58;
    
    return -1;
}


string CrawlOiData(string url, int timeout = 30) {
    char result[], data[];
    string headers, cookie = NULL;

    int rtn = WebRequest("GET", url, cookie, NULL, timeout, data, 0, result, headers);
/*
    int filehandle=FileOpen("cme.txt",FILE_WRITE|FILE_TXT);
    if(filehandle!=INVALID_HANDLE) {
         //--- Save the contents of the result[] array to a file 
         FileWriteArray(filehandle,result,0,ArraySize(result)); 
         FileWriteString(filehandle, CharArrayToString(result));
         //--- Close the file 
         FileClose(filehandle); 
    }
*/
    return CharArrayToString(result);
}


void ParseOiData(string currency, string oi, string price, OiDataStruct& oiData[]) {
    JSONParser* parser = new JSONParser();
    JSONValue* jv = parser.parse(oi);
    if(jv == NULL) {
        Print("Parse OI data error. ", CompileErrorMessage(GetLastError()));
        delete parser;
        delete jv;
        return;
    }
    
    if(!jv.isObject()) {
        delete parser;
        delete jv;
        return;
    }

    JSONObject* jo = jv;

    int idx;
    string tradeDate = jo.getString("tradeDate");
    JSONObject* totals = jo.getObject("totals");
    int totalAtClose = (int)Replace(totals.getString("atClose"), ",", "");
    int totalChange = (int)Replace(totals.getString("change"), ",", "");
    ArrayResize(oiData, ArraySize(oiData) + 1, 25);
    idx = ArraySize(oiData) - 1;
    oiData[idx].currency = currency;
    oiData[idx].tradeDate = tradeDate;
    oiData[idx].monthId = 0;
    oiData[idx].month = "total";
    oiData[idx].openInterest = totalAtClose;
    oiData[idx].oiChange = totalChange;
    
    JSONArray* monthData = jo.getArray("monthData");
    for(int i = 0; i < monthData.size(); i++) {
        ArrayResize(oiData, ArraySize(oiData) + 1, 25);
        idx = ArraySize(oiData) - 1;
        oiData[idx].currency = currency;
        oiData[idx].tradeDate = tradeDate;
        oiData[idx].monthId = i + 1;
        oiData[idx].month = monthData.getObject(i).getString("month");
        oiData[idx].openInterest = (int)Replace(monthData.getObject(i).getString("atClose"), ",", "");
        oiData[idx].oiChange = (int)Replace(monthData.getObject(i).getString("change"), ",", "");
    }


    //merge price data
    delete parser;
    delete jv;
    parser = new JSONParser();
    jv = parser.parse(price);
    if(jv == NULL) {
        Print("Parse OI price error. ", CompileErrorMessage(GetLastError()));
        delete parser;
        delete jv;
        return;
    }
    
    if(!jv.isObject()) {
        delete parser;
        delete jv;
        return;
    }
    //delete jo;
    jo = jv;
    tradeDate = jo.getString("tradeDate");
    tradeDate = StringSubstr(tradeDate, 6, 4) + StringSubstr(tradeDate, 0, 2) + StringSubstr(tradeDate, 3, 2);
    JSONArray* settlements = jo.getArray("settlements");
    OiDataStruct oiPrice;
    for(int i = 0; i < settlements.size(); i++) {
        if(settlements.getObject(i).getString("month") == "Total")  continue;
        
        oiPrice.currency = currency;
        oiPrice.tradeDate = tradeDate;
        oiPrice.month = settlements.getObject(i).getString("month");
        oiPrice.open = (double)Replace(Replace(Replace(settlements.getObject(i).getString("open"), "A", ""), "B", ""), "-", "0");
        oiPrice.close = (double)Replace(Replace(Replace(settlements.getObject(i).getString("last"), "A", ""), "B", ""), "-", "0");
        oiPrice.high = (double)Replace(Replace(Replace(settlements.getObject(i).getString("high"), "A", ""), "B", ""), "-", "0");
        oiPrice.low = (double)Replace(Replace(Replace(settlements.getObject(i).getString("low"), "A", ""), "B", ""), "-", "0");
        
        MergeOiData(oiPrice, oiData);
    }

    delete parser;
    delete jv;
}

void MergeOiData(OiDataStruct& oiData, OiDataStruct& allData[]) {
    //find existed data record then merge price into it
    for(int i = 0; i < ArraySize(allData); i++) {
        if(allData[i].currency == oiData.currency &&
           allData[i].tradeDate == oiData.tradeDate &&
           allData[i].month == oiData.month) {
            allData[i].open = oiData.open;
            allData[i].close = oiData.close;
            allData[i].high = oiData.high;
            allData[i].low = oiData.low;
            return;
        }
    }

    //add new record to allData 
    ArrayResize(allData, ArraySize(allData) + 1, 25);
    int idx = ArraySize(allData) - 1;
    allData[idx].currency = oiData.currency;
    allData[idx].tradeDate = oiData.tradeDate;
    allData[idx].month = oiData.month;
    allData[idx].open = oiData.open;
    allData[idx].close = oiData.close;
    allData[idx].high = oiData.high;
    allData[idx].low = oiData.low;
}


void WriteOiToDB(string dbFile, OiDataStruct& oiData[]) {
    //initialize SQLite
    if (!sqlite_init()) {
        Print("Unable to initialize SQLite.");
        return;
    }
    
    //create table if not existed
    int res = sqlite_table_exists(dbFile, CME_TABLE_NAME);
    if(res <= 0) {
        string createSql = "CREATE TABLE CmeOi (" +
                           " TradeDate VARCHAR(8) NULL" +
                           ",Currency VARCHAR(10) NULL" +
                           ",MonthID INTEGER NULL" +
                           ",Month VARCHAR(10) NULL" +
                           ",OpenInterest INTEGER NULL" +
                           ",OiChange INTEGER NULL" +
                           ",Open DECIMAL(10, 5) NULL" +
                           ",High DECIMAL(10, 5) NULL" +
                           ",Low DECIMAL(10, 5) NULL" +
                           ",Close DECIMAL(10, 5) NULL)";
        if(sqlite_exec(dbFile, createSql) != 0) {
            Print("Create CmeOi table failed.");
            return;
        }
    }
    
    //delete existed data
    string sqlStmt = "delete from CmeOi" + 
                     " where currency = '" + oiData[0].currency + "'" +
                     " and tradeDate = '" + oiData[0].tradeDate + "'";
    if(sqlite_exec(dbFile, sqlStmt) != 0) {
        Print("Delete CmeOi data failed.");
        return;
    }
    
    //insert new OI data
    sqlStmt = "insert into CmeOi(TradeDate, Currency, MonthID, Month, OpenInterest, OiChange, Open, High, Low, Close) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    int cols[1];
    int handle = sqlite_query(dbFile, sqlStmt, cols);
    if (handle < 0) {
        Print ("Preparing query failed.");
        return;
    }

    for(int i = 0; i < ArraySize(oiData); i++) {
        /*
        PrintFormat("currency: %s tradeData: %s monthId: %d month: %s oi: %d change: %d open: %.5f close: %.5f high: %.5f low: %.5f", 
                    oiData[i].currency,
                    oiData[i].tradeDate,
                    oiData[i].monthId,
                    oiData[i].month,
                    oiData[i].openInterest,
                    oiData[i].oiChange,
                    oiData[i].open,
                    oiData[i].close,
                    oiData[i].high,
                    oiData[i].low
                   );
        */
        sqlite_reset (handle);
        sqlite_bind_text(handle, 1, oiData[i].tradeDate + "");
        sqlite_bind_text(handle, 2, oiData[i].currency);
        sqlite_bind_int(handle, 3, oiData[i].monthId);
        sqlite_bind_text(handle, 4, oiData[i].month);
        sqlite_bind_int(handle, 5, oiData[i].openInterest);
        sqlite_bind_int(handle, 6, oiData[i].oiChange);
        sqlite_bind_double(handle, 7, oiData[i].open);
        sqlite_bind_double(handle, 8, oiData[i].high);
        sqlite_bind_double(handle, 9, oiData[i].low);
        sqlite_bind_double(handle, 10, oiData[i].close);
        sqlite_next_row (handle);
    }
    sqlite_free_query (handle);


    sqlite_finalize();
}