﻿#property copyright "Copyright 2017, Tim Hsu"
#property link      ""
#property version   "1.1"
#property description "Tim's Average True Range Indicator"
#property strict

//indicator settings
#property indicator_separate_window
#property indicator_buffers 5
#property indicator_plots   5
//ATR
#property indicator_label1  "ATR"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrDodgerBlue
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1
//TR
#property indicator_label2  "STD"
#property indicator_type2   DRAW_LINE
#property indicator_color2  clrDarkOrange
#property indicator_style2  STYLE_SOLID
#property indicator_width2  1
//STD
#property indicator_label3  "TR"
#property indicator_type3   DRAW_NONE
#property indicator_color3  clrDodgerBlue
#property indicator_style3  STYLE_DOT
#property indicator_width3  1
//ATR2
#property indicator_type4   DRAW_NONE
#property indicator_color4  clrWhite
#property indicator_style4  STYLE_DOT
#property indicator_width4  1
//TR2
#property indicator_type5   DRAW_NONE
#property indicator_color5  clrWhite
#property indicator_style5  STYLE_DOT
#property indicator_width5  1

input int PERIODS = 20;   //區間範圍

//for ATR calculation
double ExtATRBuffer[];
double ExtTRBuffer[];
//for ATR STD calculation
double ExtATR2Buffer[];
double ExtTR2Buffer[];
double ExtSTDBuffer[];

int OnInit() {
    IndicatorShortName("Average True Range");
    
    IndicatorDigits(Digits);

    
    SetIndexBuffer(0, ExtATRBuffer);
    SetIndexBuffer(1, ExtSTDBuffer);
    SetIndexBuffer(2, ExtTRBuffer);
    SetIndexBuffer(3, ExtATR2Buffer);
    SetIndexBuffer(4, ExtTR2Buffer);
    
    SetIndexLabel(0, StringFormat("ATR(%d)", PERIODS));
    SetIndexLabel(1, StringFormat("STD(%d)", PERIODS));
    SetIndexLabel(2, "TR");
    SetIndexLabel(3, NULL);
    SetIndexLabel(4, NULL);
    
    SetIndexDrawBegin(0, PERIODS);
    SetIndexDrawBegin(1, PERIODS);

    return(INIT_SUCCEEDED);
}


int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[]) {
    int i, limit;
    
    //--- check for bars count and input parameter
    if(rates_total <= PERIODS || PERIODS <= 0)  return(0);
    
    //--- counting from 0 to rates_total
    ArraySetAsSeries(ExtATRBuffer, false);
    ArraySetAsSeries(ExtTRBuffer, false);
    ArraySetAsSeries(ExtSTDBuffer, false);
    ArraySetAsSeries(ExtATR2Buffer, false);
    ArraySetAsSeries(ExtTR2Buffer, false);
    ArraySetAsSeries(open, false);
    ArraySetAsSeries(high, false);
    ArraySetAsSeries(low, false);
    ArraySetAsSeries(close, false);

    //--- preliminary calculations
    if(prev_calculated == 0) {
        ExtTRBuffer[0] = 0.0;
        ExtATRBuffer[0] = 0.0;
        ExtSTDBuffer[0] = 0.0;
        ExtTR2Buffer[0] = 0.0;
        ExtATR2Buffer[0] = 0.0;
        //--- filling out the array of True Range values for each period
        for(i = 1; i < rates_total; i++) {
            ExtTRBuffer[i] = MathMax(high[i], close[i - 1]) - MathMin(low[i], close[i - 1]);
            ExtTR2Buffer[i] = MathPow(ExtTRBuffer[i], 2);
        }
        
        //--- first AtrPeriod values of the indicator are not calculated
        double firstValue = 0.0;
        double squareValue = 0.0;
        for(i = 1; i <= PERIODS; i++) {
            ExtATRBuffer[i] = 0.0;
            ExtATR2Buffer[i] = 0.0;
            ExtSTDBuffer[i] = 0.0;
            firstValue += ExtTRBuffer[i];
            squareValue += ExtTR2Buffer[i];
        }
        
        //--- calculating the first value of the indicator
        //STD = 開根號(「平方和的平均」減去「平均的平方」)
        ExtATRBuffer[PERIODS] = firstValue / PERIODS;
        ExtATR2Buffer[PERIODS] = squareValue / PERIODS;
        ExtSTDBuffer[PERIODS] = MathSqrt(ExtATR2Buffer[PERIODS] - MathPow(ExtATRBuffer[PERIODS], 2));
        limit = PERIODS + 1;

    } else {
        limit = prev_calculated - 1;
    }

    //--- the main loop of calculations
    for(i = limit; i < rates_total; i++) {
        ExtTRBuffer[i] = MathMax(high[i], close[i - 1]) - MathMin(low[i], close[i - 1]);
        ExtTR2Buffer[i] = MathPow(ExtTRBuffer[i], 2);
        ExtATRBuffer[i] = ExtATRBuffer[i - 1] + (ExtTRBuffer[i] - ExtTRBuffer[i - PERIODS]) / PERIODS;
        ExtATR2Buffer[i] = ExtATR2Buffer[i - 1] + (ExtTR2Buffer[i] - ExtTR2Buffer[i - PERIODS]) / PERIODS;
        ExtSTDBuffer[i] = MathSqrt(ExtATR2Buffer[i] - MathPow(ExtATRBuffer[i], 2));
    }
    
    //--- return value of prev_calculated for next call
    return rates_total;
}
