package com.timhsu.forextools.persistent;

import com.timhsu.forextools.persistent.auto._ForexDatabaseMap;

public class ForexDatabaseMap extends _ForexDatabaseMap {

    private static ForexDatabaseMap instance;

    private ForexDatabaseMap() {}

    public static ForexDatabaseMap getInstance() {
        if(instance == null) {
            instance = new ForexDatabaseMap();
        }

        return instance;
    }
}
