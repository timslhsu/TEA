package com.timhsu.forextools.commands;

import com.timhsu.forextools.ForexTool;
import com.timhsu.forextools.persistent.TestOrderHistory;
import com.timhsu.forextools.persistent.TestOrderHistoryTransformed;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.query.SQLTemplate;
import org.apache.cayenne.query.SelectQuery;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class TransformTestOrderHistory implements Command {
    private String inputFile = ForexTool.getProperty("TransformTestOrderHistory.InputFile", "TestReport.htm");
    private String outputFile = ForexTool.getProperty("TransformTestOrderHistory.OutputFile", "TradeRecords.csv");

    public int run(String[] args) throws Exception {
        if (args.length > 0 && args[0].toUpperCase().equals("HELP")) {
            System.out.println("Usage: "
                    + ForexTool.class.getSimpleName() + " "
                    + TransformTestOrderHistory.class.getSimpleName() + " <TestReport.htm> <Output.csv>");
            System.out.println("");
            return 0;
        }

        if (args.length > 1) inputFile = args[0];
        if (args.length > 2) inputFile = args[1];

        try {
            loadTestOrderHistory(inputFile);
            transformTestOrderHistory();
            exportTransformedOrderHistory(outputFile);

        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }

        return 0;
    }

    private void loadTestOrderHistory(String testReportFile) {
        File report = new File(testReportFile);

        Document doc;
        try {
            doc = Jsoup.parse(report, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        ObjectContext context = ForexTool.getCayenneContext();

        //delete all data in working tables
        //List<TestOrderHistory> objs;
        //objs = context.performQuery(new SelectQuery(TestOrderHistory.class));
        context.deleteObjects(context.performQuery(new SelectQuery(TestOrderHistory.class)));
        //objs = context.performQuery(new SelectQuery(TestOrderHistoryTransformed.class));
        context.deleteObjects(context.performQuery(new SelectQuery(TestOrderHistoryTransformed.class)));
        context.commitChanges();

        String symbol = doc.getElementsByTag("table").get(0)
                .getElementsByTag("tr").get(0)
                .getElementsByTag("td").get(1).text()
                .split(" ")[0];

        Elements rows = doc.getElementsByTag("table").get(1).getElementsByTag("tr");
        Element row;
        TestOrderHistory order;
        for (int i = 1; i < rows.size(); i++) {
            row = rows.get(i);
            order = context.newObject(TestOrderHistory.class);
            order.setSymbol(symbol);
            order.setEventId(Integer.parseInt(row.child(0).text()));
            order.setEventDate(row.child(1).text().split(" ")[0]);
            order.setEventTime(row.child(1).text().split(" ")[1] + ":00");
            order.setEventType(row.child(2).text());
            order.setTicket(Integer.parseInt(row.child(3).text()));
            order.setLots(Double.parseDouble(row.child(4).text()));
            order.setPrice(Double.parseDouble(row.child(5).text()));
            order.setStopLoss(Double.parseDouble(row.child(6).text()));
            order.setTakeProfit(Double.parseDouble(row.child(7).text()));
            if (row.child(8).text().equals("")) {
                order.setProfit(0d);
            } else {
                order.setProfit(Double.parseDouble(row.child(8).text()));
            }
        }
        context.commitChanges();
    }

    private void transformTestOrderHistory() {
        String sql = "insert into TestOrderHistoryTransformed(ticket, symbol, orderType, openDate, openTime, openPrice, closeDate, closeTime, closePrice, lots, profit, takeProfit, stopLoss) " +
                "select open.ticket, symbol, case when orderType = 'buy' then 0 when orderType = 'sell' then 1 end orderType, openDate, openTime, openPrice, closeDate, closeTime, closePrice, lots, profit, takeProfit, stopLoss " +
                "from (" +
                "select ticket, symbol, eventDate openDate, eventTime openTime, eventType orderType, lots, price openPrice, takeProfit, stopLoss " +
                "from TestOrderHistory " +
                "where eventType in ('sell', 'buy')" +
                ") open join (" +
                "select ticket, eventDate closeDate, eventTime closeTime, price closePrice, profit " +
                "from TestOrderHistory " +
                "where eventType = 'close'" +
                ") close " +
                "on open.ticket = close.ticket " +
                "order by open.ticket";

        ObjectContext context = ForexTool.getCayenneContext();
        SQLTemplate orders = new SQLTemplate(TestOrderHistoryTransformed.class, sql);
        context.performGenericQuery(orders);
        context.commitChanges();
    }

    private void exportTransformedOrderHistory(String outputFile) throws IOException {
        FileWriter fw = new FileWriter(outputFile);
        CSVPrinter csvWriter = new CSVPrinter(fw, CSVFormat.DEFAULT);
        ObjectContext context = ForexTool.getCayenneContext();
        SelectQuery qry = new SelectQuery(TestOrderHistoryTransformed.class);
        List<TestOrderHistoryTransformed> orders = context.performQuery(qry);

        csvWriter.printRecord("Account", "Ticket", "Symbol", "OrderType", "OpenTime", "OpenPrice", "CloseTime", "ClosePrice", "Lots", "Profit", "TakeProfit", "StopLoss", "Swap", "Commission", "Comment", "MagicNumber");
        for (TestOrderHistoryTransformed ord : orders) {
            csvWriter.printRecord(
                    "TestAccount",
                    ord.getTicket(),
                    ord.getSymbol(),
                    ord.getOrderType(),
                    ord.getOpenDate() + " " + ord.getOpenTime(),
                    ord.getOpenPrice(),
                    ord.getCloseDate() + " " + ord.getCloseTime(),
                    ord.getClosePrice(),
                    ord.getLots(),
                    ord.getProfit(),
                    ord.getTakeProfit(),
                    ord.getStopLoss(),
                    ord.getSwap(),
                    ord.getCommission(),
                    ord.getComment(),
                    ord.getMagicNumber());
        }

        csvWriter.flush();
        csvWriter.close();
        fw.close();
    }
}