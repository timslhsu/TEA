package com.timhsu.forextools.commands;

import com.timhsu.forextools.ForexTool;
import com.timhsu.forextools.persistent.FinanceCalendar;
import com.timhsu.forextools.utils.ChineseConverter;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.SelectQuery;
import org.apache.cayenne.query.SortOrder;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import java.io.*;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class CrawlFinanceCalendar implements Command {
    //https://rili.jin10.com/?date=20170309
    private static final String JIN10_END_POINT = "https://rili.jin10.com/?date=%s";

    private int crawlingDays = Integer.parseInt(ForexTool.getProperty("CrawlFinanceCalendar.CrawlingDays", "10"));
    private String outputFile = ForexTool.getProperty("MT4.SharedFolder", ".") +
            File.separator +
            ForexTool.getProperty("CrawlFinanceCalendar.OutputFile", "FinanceCalendar.csv");
    private int importanceFilter = Integer.parseInt(ForexTool.getProperty("CrawlFinanceCalendar.ImportanceFilter", "80"));
    //private String countryFilter = ForexTool.getProperty("CrawlFinanceCalendar.CountryFilter", "");
    private String countryCurrencyMap = ForexTool.getProperty("CrawlFinanceCalendar.CountryCurrencyMap", "美國:USD,德國:EUR,英國:GBP,中國:CNY,法國:EUR,歐元區:EUR,瑞士:CHF,日本:JPY,澳大利亞:AUD,加拿大:CAD,奧地利:EUR");
    private HashMap<String, String> countryToCurrency = new HashMap<String, String>();

    public int run(String[] args) throws Exception {
        if (args.length > 0 && args[0].toUpperCase().equals("HELP")) {
            System.out.println("Usage: "
                    + ForexTool.class.getSimpleName() + " "
                    + CrawlFinanceCalendar.class.getSimpleName() + " <Output.csv> [<StartDate> <EndDate>]");
            System.out.println("");
            return -1;
        }

        if (args.length > 1)
            outputFile = ForexTool.getProperty("MT4.SharedFolder", "")
                    + File.separator
                    + args[0];

        String mapEntry[] = countryCurrencyMap.split(",");
        for (String entry : mapEntry) {
            String pair[] = entry.split(":");
            if (pair.length == 2)
                countryToCurrency.put(pair[0].trim(), pair[1].trim().toUpperCase());
        }

        try {
            Calendar startDate;
            Calendar endDate;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

            if (args.length == 3) {
                Date start = sdf.parse(args[1]);
                startDate = Calendar.getInstance();
                startDate.setTime(start);
                Date end = sdf.parse(args[2]);
                endDate = Calendar.getInstance();
                endDate.setTime(end);

            } else {
                startDate = Calendar.getInstance();
                endDate = Calendar.getInstance();
                endDate.add(Calendar.DAY_OF_MONTH, crawlingDays);
            }

            String dateToCrawl = sdf.format(startDate.getTime());
            String endDateToCrawl = sdf.format(endDate.getTime());

            while (Integer.parseInt(dateToCrawl) <= Integer.parseInt(endDateToCrawl)) {
                String pageSource = crawlFinanceCalendar(dateToCrawl);
                parseFinaceCalendar(pageSource);

                startDate.add(Calendar.DAY_OF_MONTH, 1);
                dateToCrawl = sdf.format(startDate.getTime());
            }
            exportFinanceCalendar(outputFile);

        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }

        return 0;
    }

    public String crawlFinanceCalendar(String dateToCrawl) {
        FirefoxProfile prof = new FirefoxProfile();
        prof.setPreference("general.useragent.override", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:51.0) Gecko/20100101 Firefox/51.0");
        WebDriver driver = new FirefoxDriver(prof);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get(String.format(JIN10_END_POINT, dateToCrawl));
        driver.findElement(By.cssSelector("a.jin-rili_filter.J_riliFilterBtn > span")).click();
        driver.findElement(By.linkText("重要")).click();
        String resp = driver.getPageSource();
        driver.quit();

        return resp;
    }

    public void parseFinaceCalendar(String pageSource) {
        ArrayList<CalendarEvent> allEvents = new ArrayList<CalendarEvent>();
        Document doc = Jsoup.parse(pageSource);

        String eventDate = doc.select(".J_riliDateTitle").get(0).text();
        eventDate = eventDate.replace("年", "").replace("月", "").replace("日", "");

        //parse finance indicator
        Elements rows = doc.select("tbody#J_economicsWrap>tr");
        String eventTime = "";
        String country = "";
        String eventType = "Indicator";
        CalendarEvent event;
        for (Element r : rows) {
            if (r.select("td").size() == 9) {
                eventTime = r.select("td.jin-rili_content-time").text();
                country = ChineseConverter.StoT(r.select("td.jin-rili_content-country>img").attr("src").replace("//cdn.jin10.com/assets/img/commons/flag/", "").replace(".png", ""));
                event = new CalendarEvent();
                event.eventType = eventType;
                event.eventDate = eventDate;
                event.eventTime = eventTime;
                event.country = country;
                event.currency = countryToCurrency.get(country);
                event.eventDesc = ChineseConverter.StoT(r.select("td").get(2).text());
                event.importance = Integer.parseInt(r.select("td>div.jin-star>i").attr("style").replace("width:", "").replace("%;", ""));
                event.priorValue = r.select("td").get(4).text();
                event.estimatedValue = r.select("td").get(5).text();
                event.publishedValue = r.select("td").get(6).text();

            } else if (r.select("td").size() == 7) {
                event = new CalendarEvent();
                event.eventType = eventType;
                event.eventDate = eventDate;
                event.eventTime = eventTime;
                event.country = country;
                event.currency = countryToCurrency.get(country);
                event.eventDesc = ChineseConverter.StoT(r.select("td").get(0).text());
                event.importance = Integer.parseInt(r.select("td>div.jin-star>i").attr("style").replace("width:", "").replace("%;", ""));
                event.priorValue = r.select("td").get(2).text();
                event.estimatedValue = r.select("td").get(3).text();
                event.publishedValue = r.select("td").get(4).text();

            } else {
                continue;
            }

            allEvents.add(event);
//            System.out.print(event.eventType + " _ ");
//            System.out.print(event.eventDate + " _ ");
//            System.out.print(event.eventTime + " _ ");
//            System.out.print(event.country + " _ ");
//            System.out.print(event.importance + " _ ");
//            System.out.print(event.eventDesc + " _ ");
//            System.out.print(event.priorValue + " _ ");
//            System.out.print(event.estimatedValue + " _ ");
//            System.out.println(event.publishedValue);
        }


        //parse finance activity
        rows = doc.select("tbody#J_eventWrap>tr");
        eventTime = "";
        country = "";
        eventType = "Event";
        for (Element r : rows) {
            if (r.select("td").size() == 5) {
                eventTime = r.select("td.jin-rili_content-time").text();
                country = ChineseConverter.StoT(r.select("td").get(1).text().trim());
                event = new CalendarEvent();
                event.eventType = eventType;
                event.eventDate = eventDate;
                event.eventTime = eventTime;
                event.country = country;
                event.currency = countryToCurrency.get(country);
                event.importance = Integer.parseInt(r.select("td>div.jin-star>i").attr("style").replace("width:", "").replace("%;", ""));
                int size = r.select("td>div>p").size();
                if (size > 0) {
                    event.eventDesc = ChineseConverter.StoT(r.select("td>div>p").get(size - 1).text());
                }
            } else {
                continue;
            }

            allEvents.add(event);
//            System.out.print(event.eventType + " _ ");
//            System.out.print(event.eventDate + " _ ");
//            System.out.print(event.eventTime + " _ ");
//            System.out.print(event.country + " _ ");
//            System.out.print(event.importance + " _ ");
//            System.out.println(event.eventDesc);
        }


        //insert to database
        ObjectContext context = ForexTool.getCayenneContext();
        SelectQuery qry = new SelectQuery(FinanceCalendar.class);
        qry.setQualifier(ExpressionFactory.matchExp(FinanceCalendar.EVENT_DATE_PROPERTY, eventDate));
        context.deleteObjects(context.performQuery(qry));

        for (CalendarEvent e : allEvents) {
            FinanceCalendar fc = context.newObject(FinanceCalendar.class);
            fc.setEventType(e.eventType);
            fc.setEventDate(e.eventDate);
            fc.setEventTime(e.eventTime);
            fc.setCountry(e.country);
            fc.setCurrency(e.currency);
            fc.setImportance(e.importance);
            fc.setEventDesc(e.eventDesc);
            fc.setPriorValue(e.priorValue);
            fc.setEstimatedValue(e.estimatedValue);
            fc.setPublishedValue(e.publishedValue);
        }

        context.commitChanges();
    }

    public void exportFinanceCalendar(String outputFile) throws IOException {
        //FileWriter fw = new FileWriter(outputFile);
        OutputStreamWriter fw = new OutputStreamWriter(new FileOutputStream(outputFile), Charset.forName("UTF-8"));
        CSVPrinter csvWriter = new CSVPrinter(fw, CSVFormat.DEFAULT);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String startDate = sdf.format(Calendar.getInstance().getTime());

        ObjectContext context = ForexTool.getCayenneContext();
        SelectQuery qry = new SelectQuery(FinanceCalendar.class);
        qry.setQualifier(ExpressionFactory.greaterOrEqualExp(FinanceCalendar.EVENT_DATE_PROPERTY, startDate));
        qry.andQualifier(ExpressionFactory.greaterOrEqualExp(FinanceCalendar.IMPORTANCE_PROPERTY, importanceFilter));
//        if (!countryFilter.isEmpty()) {
//            qry.andQualifier(ExpressionFactory.inExp(FinanceCalendar.COUNTRY_PROPERTY, Arrays.asList(countryFilter.split(","))));
//        }
        qry.addOrdering("eventDate", SortOrder.ASCENDING);
        qry.addOrdering("eventTime", SortOrder.ASCENDING);

        List<FinanceCalendar> allEvents = context.performQuery(qry);
        csvWriter.printRecord("Date", "Time", "Country", "Currency", "Event");
        for (FinanceCalendar fc : allEvents) {
            csvWriter.printRecord(
                    fc.getEventDate().substring(0, 4) + "." + fc.getEventDate().substring(4, 6) + "." + fc.getEventDate().substring(6, 8),
                    fc.getEventTime(),
                    fc.getCountry(),
                    fc.getCurrency(),
                    fc.getEventDesc()
            );
        }

        csvWriter.flush();
        csvWriter.close();
        fw.close();
    }

    private class CalendarEvent {
        private String eventType;
        private String eventDate;
        private String eventTime;
        private String country;
        private String currency;
        private int importance;
        private String eventDesc;
        private String priorValue;
        private String estimatedValue;
        private String publishedValue;
    }
}
