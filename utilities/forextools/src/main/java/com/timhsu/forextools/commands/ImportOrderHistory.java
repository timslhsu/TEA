package com.timhsu.forextools.commands;

import com.timhsu.forextools.ForexTool;
import com.timhsu.forextools.persistent.OrderHistory;
import org.apache.cayenne.Cayenne;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.exp.ExpressionFactory;
import org.apache.cayenne.query.SelectQuery;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;

public class ImportOrderHistory implements Command {

    public int run(String[] args) throws Exception {
        if (args.length == 0) {
            System.out.println("Usage: "
                    + ForexTool.class.getSimpleName() + " "
                    + ImportOrderHistory.class.getSimpleName() + " <OrderHistory.csv>");
            System.out.println("");
            return -1;
        }

        try {
            parseCsvStatement(args[0]);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }

        return 0;
    }

    private void parseCsvStatement(String statementCSV) {

        try {
            FileReader fr = new FileReader(statementCSV);
            CSVParser csvReader = new CSVParser(fr, CSVFormat.DEFAULT.withHeader());
            ObjectContext context = ForexTool.getCayenneContext();

            for (CSVRecord r : csvReader) {
                SelectQuery qry = new SelectQuery(OrderHistory.class);
                qry.setQualifier(ExpressionFactory.matchExp(OrderHistory.ACCOUNT_PROPERTY, r.get("Account")));
                qry.andQualifier(ExpressionFactory.matchExp(OrderHistory.TICKET_PROPERTY, r.get("Ticket")));
                OrderHistory txn = (OrderHistory) Cayenne.objectForQuery(context, qry);

                if (txn == null) {
                    txn = context.newObject(OrderHistory.class);
                    txn.setAccount(r.get("Account"));
                    txn.setTicket(Integer.parseInt(r.get("Ticket")));
                }

                txn.setSymbol(r.get("Symbol"));
                txn.setOrderType(Integer.parseInt(r.get("OrderType")));
                txn.setOpenDate(r.get("OpenTime").split(" ")[0].replace(".", ""));
                txn.setOpenTime(r.get("OpenTime").split(" ")[1].replace(":", ""));
                txn.setOpenPrice(Double.parseDouble(r.get("OpenPrice")));
                txn.setCloseDate(r.get("CloseTime").split(" ")[0].replace(".", ""));
                txn.setCloseTime(r.get("CloseTime").split(" ")[1].replace(":", ""));
                txn.setClosePrice(Double.parseDouble(r.get("ClosePrice")));
                txn.setLots(Double.parseDouble(r.get("Lots")));
                txn.setProfit(Double.parseDouble(r.get("Profit")));
                txn.setTakeProfit(Double.parseDouble(r.get("TakeProfit")));
                txn.setStopLoss(Double.parseDouble(r.get("StopLoss")));
                txn.setSwap(Double.parseDouble(r.get("Swap")));
                txn.setCommission(Double.parseDouble(r.get("Commission")));
                txn.setComment(r.get("Comment"));
                txn.setMagicNumber(Integer.parseInt(r.get("MagicNumber")));

                context.commitChanges();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
